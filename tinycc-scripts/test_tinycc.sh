#!/bin/bash

set -e

VARIANT="$1"

tar czf /tmp/testbed.tar.gz .
mv /tmp/testbed.tar.gz .

wget -nv -O artifacts.zip https://gitlab.com/api/v4/projects/giomasce%2Ftinycc-test/jobs/artifacts/master/download?job=build_$VARIANT
unzip artifacts.zip
chmod 600 artifacts/ssh_user_*_key

screen -d -m ./launch_image.sh "$VARIANT"

for i in `seq 10` ; do
    sleep 5
    set +e
    ssh -o StrictHostKeyChecking=false -o ConnectTimeout=5 -p2222 -i artifacts/ssh_user_rsa_key root@localhost bash -c 'cat > /tmp/testbed.tar.gz && cd /tmp && tar xzf testbed.tar.gz && ./testbed.sh' < testbed.tar.gz
    SSH_RET=$?
    set -e
    if [ $SSH_RET -eq 255 ] ; then
        echo "SSH failed..."
    else
        echo "SSH returned $SSH_RET"
        #while [ ! -f vm-exited ] ; do
        #    sleep 1
        #done
        #echo "VM exited"
        #echo "=== VM serial log ==="
        #cat serial.log
        #echo "=== end VM serial log ==="
        exit $SSH_RET
    fi
done

echo "Retry number exceeded, failing..."
exit 1

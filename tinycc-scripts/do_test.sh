#!/bin/bash

set -e

./configure
make
make test
make install

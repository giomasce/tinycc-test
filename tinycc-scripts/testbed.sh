#!/bin/bash

set -e

if ./do_test.sh ; then
    echo "Tests for commit `cat original-commit` succeded!"
    exit 0
else
    echo "Tests for commit `cat original-commit` failed..."
    exit 1
fi

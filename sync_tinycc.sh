#!/bin/bash

set -e

cd tinycc
git fetch origin
git reset -q --hard origin/mob
git clean -fdx
git rev-parse HEAD > ../new-commit
touch ../old-commit

if [ "`cat ../old-commit`" != "`cat ../new-commit`" ] ; then
    cp ../new-commit ../old-commit
    cp ../new-commit original-commit
    cp -a ../tinycc-scripts/* .
    mv gitlab-ci.yml .gitlab-ci.yml
    cp -a ../launch_image.sh .
    git add .
    git commit -a --amend --no-edit
    git push -f gl test
fi

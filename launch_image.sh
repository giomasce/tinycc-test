#!/bin/bash

set -e

VARIANT="$1"

if [ "$VARIANT" == "i386-pc" ] ; then
    qemu-system-i386 -machine pc -cpu coreduo -m 1G -drive file=artifacts/image.qcow2 -device e1000,netdev=net -netdev user,id=net,hostfwd=tcp::2222-:22 -kernel artifacts/kernel -initrd artifacts/initrd -nographic -append "root=LABEL=rootfs console=ttyS0" -serial file:serial.log

elif [ "$VARIANT" == "amd64-pc" ] ; then
    qemu-system-x86_64 -machine pc -cpu Nehalem -m 1G -drive file=artifacts/image.qcow2 -device e1000,netdev=net -netdev user,id=net,hostfwd=tcp::2222-:22 -kernel artifacts/kernel -initrd artifacts/initrd -nographic -append "root=LABEL=rootfs console=ttyS0" -serial file:serial.log

elif [ "$VARIANT" == "armhf-virt" ] ; then
    qemu-system-arm -machine virt -cpu cortex-a15 -m 1G -device virtio-blk-device,drive=hd -drive file=artifacts/image.qcow2,if=none,id=hd -device virtio-net-device,netdev=net -netdev user,id=net,hostfwd=tcp::2222-:22 -kernel artifacts/kernel -initrd artifacts/initrd -nographic -append "root=LABEL=rootfs console=ttyAMA0" -serial file:serial.log

elif [ "$VARIANT" == "arm64-virt" ] ; then
    qemu-system-aarch64 -machine virt -cpu cortex-a57 -m 1G -device virtio-blk-device,drive=hd -drive file=artifacts/image.qcow2,if=none,id=hd -device virtio-net-device,netdev=net -netdev user,id=net,hostfwd=tcp::2222-:22 -kernel artifacts/kernel -initrd artifacts/initrd -nographic -append "root=LABEL=rootfs console=ttyAMA0" -serial file:serial.log

elif [ "$VARIANT" == "riscv64-virt" ] ; then
    qemu-system-riscv64 -machine virt -cpu rv64 -m 1G -device virtio-blk-device,drive=hd -drive file=artifacts/image.qcow2,if=none,id=hd -device virtio-net-device,netdev=net -netdev user,id=net,hostfwd=tcp::2222-:22 -kernel /usr/lib/riscv64-linux-gnu/opensbi/qemu/virt/fw_jump.elf -device loader,file=/usr/lib/u-boot/qemu-riscv64_smode/u-boot.bin,addr=0x80200000 -object rng-random,filename=/dev/urandom,id=rng -device virtio-rng-device,rng=rng -nographic -append "root=LABEL=rootfs console=ttyS0" -serial file:serial.log

else
    echo "Variant not found"
    touch vm-exited
    exit 1
fi

touch vm-exited
exit 0

#!/bin/bash

set -e

VARIANT="$1"

wget -nv -O artifacts.zip https://gitlab.com/api/v4/projects/giomasce%2Fdqib/jobs/artifacts/master/download?job=convert_$VARIANT
unzip artifacts.zip
chmod 600 artifacts/ssh_user_*_key

screen -d -m ./launch_image.sh "$VARIANT"

tar czf install_image.tar.gz install_image.sh

for i in `seq 10` ; do
    sleep 5
    set +e
    ssh -o StrictHostKeyChecking=false -o ConnectTimeout=5 -p2222 -i artifacts/ssh_user_rsa_key root@localhost bash -c 'cat > /tmp/install_image.tar.gz && mkdir /tmp/test && cd /tmp/test && tar xzf /tmp/install_image.tar.gz && ./install_image.sh' < install_image.tar.gz
    SSH_RET=$?
    set -e
    if [ $SSH_RET -eq 255 ] ; then
        echo "SSH failed..."
    else
        echo "SSH returned $SSH_RET"
        while [ ! -f vm-exited ] ; do
            sleep 1
        done
        echo "VM exited"
        #echo "=== VM serial log ==="
        #cat serial.log
        #echo "=== end VM serial log ==="
        exit $SSH_RET
    fi
done

echo "Retry number exceeded, failing..."
exit 1
